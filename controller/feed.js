exports.getPosts = (req, res, next) => {
    res.status(200) 
        .json({posts: [
                {title: 'Post 1', content: 'abc'}, 
                {title: 'Post 2', content: 'def'}
            ]
        });
};

exports.postPost = (req, res, next) => {
    // post content
    const title = req.body.title;
    const content = req.body.content;
    
    res.status(201)
        .json({
            message: 'Post created successfully',
            post: {
                id: new Date().toISOString(),
                title: title,
                content: content
            }
        });
        
};
