const express = require('express');

const feedController = require('../controller/feed');

const router = express.Router();

// GET /feed/posts
router.get('/posts', feedController.getPosts);
// POST /feed/posts
router.post('/posts', feedController.postPost);

module.exports = router;
