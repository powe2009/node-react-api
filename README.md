https://www.udemy.com/course/nodejs-the-complete-guide/learn/lecture/12097858#overview

git init --yes

npm i express
npm i --save-dev nodemon
npm i body-parser

https://codepen.io/
    html
        <button id='get'>Get</button>
        <button id='post'>Post</button>
    js
        const getButton = document.getElementById('get');
        getButton.addEventListener( 'click', () => {
            fetch('http://localhost:7575/feed/posts')
            .then(res => res.json())
            .then(resJson => console.log(resJson))
            .catch(err => console.log(err))
        });

        const postButton = document.getElementById('post');
        postButton.addEventListener('click', () => {
        fetch('http://localhost:7575/feed/posts', {
            method: 'POST',
            body: JSON.stringify({
            title: 'post 3',
            content: 'abc 3'
            }),
            headers: {
            'Content-Type': 'application/json'
            }
        })
        .then(res => {
            console.log(res);
        })
        .catch(err => {
            console.log(err);
        })
        });
